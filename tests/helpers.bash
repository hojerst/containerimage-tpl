print_files() {
  for i in "$@" ; do
    {
      batcat --style=full -f "$i" 2>/dev/null ||
      xbat --style=full -f "$i" 2>/dev/null ||
      {
        echo $'\e[33m'"$i"
        cat "$i"
        echo
      }
    }
  done
}

print_tree() {
  local dir="${1:-$PWD}"
  (
    cd "$dir" || return 1
    IFS=$'\n' print_files $(find . -type f)
  )
}
